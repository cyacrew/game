// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CyacraftInteractiveInterface.generated.h"

class ACyacraftCharacter;

UINTERFACE(Blueprintable)
class CYACRAFT_API UCyacraftInteractiveInterface : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

class CYACRAFT_API ICyacraftInteractiveInterface
{
public:
	GENERATED_IINTERFACE_BODY()

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Cyacraft | Interactions")
	void OnPlayerInteraction(ACyacraftCharacter* Player);	
};
