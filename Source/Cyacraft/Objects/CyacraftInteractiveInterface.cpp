// Fill out your copyright notice in the Description page of Project Settings.

#include "Cyacraft.h"
#include "CyacraftInteractiveInterface.h"

UCyacraftInteractiveInterface::UCyacraftInteractiveInterface(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{}

void ICyacraftInteractiveInterface::OnPlayerInteraction_Implementation(ACyacraftCharacter* Player)
{
	// Does nothing
}

