// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "CyacraftAbstractBlock.generated.h"

UCLASS()
class CYACRAFT_API ACyacraftAbstractBlock : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACyacraftAbstractBlock();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
};
