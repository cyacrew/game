// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/Character.h"
#include "CyacraftCharacter.generated.h"

class UInputComponent;

UCLASS(config=Game)
class ACyacraftCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ACyacraftCharacter();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	UFUNCTION(BlueprintCallable, Category = "Cyacraft")
	bool CanAttack();

	UFUNCTION(BlueprintCallable, Category = "Cyacraft")
	void Attack();

protected:
	/** Current Health Points of the character */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cyacraft | Stats")
	int32 CurrentLife;
	/** Maximum Health the character can have */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cyacraft | Stats")
	int32 MaxLife;

	/** The unit run speed, in meters per second */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cyacraft | Stats")
	float RunSpeed;

	/** The unit walk speed, in meters per second */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cyacraft | Stats")
	float WalkSpeed;

	/** The weapon minimum damages inflicted */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cyacraft | Stats")
	float MinDamages;
	
	/** The weapon maximum damages inflicted */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cyacraft | Stats")
	float MaxDamages;

	/** The weapon range, in meters */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cyacraft | Stats")
	float Range;

	/** Time between two attacks, in seconds */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cyacraft | Stats")
	float AttackCooldown;

	/** Last time this weapon has attack*/
	FDateTime LastAttackTime;
};

