// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "Cyacraft.h"
#include "CyacraftCharacter.h"

// Sets default values
ACyacraftCharacter::ACyacraftCharacter() :
	CurrentLife(1), MaxLife(1), MinDamages(1), Range(1),
	RunSpeed(1.f), WalkSpeed(0.45f), AttackCooldown(1),
	LastAttackTime(FDateTime::UtcNow())
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACyacraftCharacter::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ACyacraftCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ACyacraftCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);
}

bool ACyacraftCharacter::CanAttack()
{
	return ((FDateTime::UtcNow() - LastAttackTime).GetTotalMilliseconds() / 1000.f) > AttackCooldown;
}

void ACyacraftCharacter::Attack()
{
	LastAttackTime = FDateTime::UtcNow();
}