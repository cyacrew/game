// Fill out your copyright notice in the Description page of Project Settings.

#include "Cyacraft.h"
#include "Player/CyacraftPlayerCharacter.h"
#include "Perception/PawnSensingComponent.h"
#include "CyacraftAIBehavior.h"
#include "CyacraftAICharacter.h"

PRAGMA_DISABLE_OPTIMIZATION_ACTUAL
// Sets default values
ACyacraftAICharacter::ACyacraftAICharacter(const FObjectInitializer& ObjectInitializer) :
	State(EAIState::Idle),
	BehaviorComponent(nullptr),
	LastSeenTime(FDateTime::UtcNow())
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SensingComponent = ObjectInitializer.CreateDefaultSubobject<UPawnSensingComponent>(this, TEXT("Senses"));
}

// Called when the game starts or when spawned
void ACyacraftAICharacter::BeginPlay()
{
	Super::BeginPlay();

	if (SensingComponent)
	{
		SensingComponent->OnSeePawn.AddDynamic(this, &ACyacraftAICharacter::OnSeePawn_Internal);
		SensingComponent->OnHearNoise.AddDynamic(this, &ACyacraftAICharacter::OnHearPawn_Internal);
	}

	if (BehaviorComponent)
		BehaviorComponent->SetAICharacter(this);
}

// Called every frame
void ACyacraftAICharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	UpdatePawnSight();
}

// Called to bind functionality to input
void ACyacraftAICharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);
}

void ACyacraftAICharacter::UpdatePawnSight()
{
	if (State == EAIState::Following || State == EAIState::Attacking)
	{
		auto Delta = (FDateTime::UtcNow() - LastSeenTime).GetMilliseconds() / 1000.f;
		if (Delta > SensingComponent->SensingInterval)
		{
			State = EAIState::Idle;
			OnLostSightOnPawn_Internal();
		}
	}
}

void ACyacraftAICharacter::OnSeePawn_Internal(APawn* Pawn)
{
	UE_LOG(LogTemp, Warning, TEXT("OnSeePawn_Internal"));
	auto Player = Cast<ACyacraftPlayerCharacter>(Pawn);
	if (!Player)
		return;

	LastSeenTime = FDateTime::UtcNow();
	if (State == EAIState::Idle)
		State = EAIState::Following;

	OnSeePlayer(Player);
}

void ACyacraftAICharacter::OnHearPawn_Internal(APawn* Pawn, const FVector& Location, float Intensity)
{
	auto Player = Cast<ACyacraftPlayerCharacter>(Pawn);
	if (!Player)
		return;

	UE_LOG(LogTemp, Warning, TEXT("OnHearPawn_Internal"));
	OnHearPlayer(Player, Location, Intensity);
}

void ACyacraftAICharacter::OnLostSightOnPawn_Internal()
{
	UE_LOG(LogTemp, Warning, TEXT("OnLostSightOnPawn_Internal"));
	OnLostSightOnPawn();
}
PRAGMA_ENABLE_OPTIMIZATION_ACTUAL