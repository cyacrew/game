// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Character/CyacraftCharacter.h"
#include "CyacraftAICharacter.generated.h"

class ACyacraftPlayerCharacter;
class UCyacraftAIBehavior;
class UPawnSensingComponent;

UENUM()
enum class EAIState : uint8
{
	Idle,
	Following,
	Attacking,
	Dying,
	Dead
};

UCLASS()
class CYACRAFT_API ACyacraftAICharacter : public ACyacraftCharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ACyacraftAICharacter(const FObjectInitializer& ObjectInitializer);

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cyacraft | AI")
	EAIState State;

	/** AI PAWN SENSE FUNCTIONS */
	UFUNCTION(BlueprintImplementableEvent, Category = "Cyacraft | AI")
	void OnSeePlayer(ACyacraftPlayerCharacter* Player);

	UFUNCTION(BlueprintImplementableEvent, Category = "Cyacraft | AI")
	void OnHearPlayer(ACyacraftPlayerCharacter* Player, const FVector& Location, float Intensity);

	UFUNCTION(BlueprintImplementableEvent, Category = "Cyacraft | AI")
	void OnLostSightOnPawn();

	/** Updates the sight state and triggers OnLostSightOnPawn if needed */
	UFUNCTION(BlueprintCallable, Category = "Cyacraft | AI")
	void UpdatePawnSight();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cyacraft | AI")
	UPawnSensingComponent* SensingComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cyacraft | AI")
	UCyacraftAIBehavior* BehaviorComponent;

private:
	UFUNCTION()
	void OnSeePawn_Internal(APawn* Pawn);
	UFUNCTION()
	void OnHearPawn_Internal(APawn* Pawn, const FVector& Location, float Intensity);
	UFUNCTION()
	void OnLostSightOnPawn_Internal();

	FDateTime LastSeenTime;
};
