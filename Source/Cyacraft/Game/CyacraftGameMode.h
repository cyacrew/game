// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "CyacraftGameMode.generated.h"

UCLASS(minimalapi)
class ACyacraftGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	ACyacraftGameMode();
};



