// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "Cyacraft.h"
#include "CyacraftGameMode.h"
#include "CyacraftHUD.h"
#include "Character/CyacraftCharacter.h"

ACyacraftGameMode::ACyacraftGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/CyaCraft/Blueprints/Characters/Character/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ACyacraftHUD::StaticClass();
}
